//
// Created by AURELIEN on 16/10/2020.
//

#include "Tetravex/SequentialSolver.h"

SequentialSolver::SequentialSolver(Board *board, Renderer *renderer) {
    this->board = board;
    this->renderer = renderer;
    P.resize(board->GetPieceCount(), nullptr);
    unsigned i = 0;
    for(auto& piece : board->GetOriginalPieces()) {
        P[i] = std::make_shared<Piece>(Piece(*piece));
        ++i;
    }
    solved = false;
}

void SequentialSolver::Solve() {
    try {
        renderer->RenderRoutine(_Name() + ": Started (may take some time during which the program is frozen)");
        b.resize(board->GetPieceCount(), nullptr);
        auto start = std::chrono::system_clock::now();
        solved = _Backtracking(0);
        auto end = std::chrono::system_clock::now();
        std::string message;
        if(!solved) {
            message = _Name() + ": Unable to generate solution";
        } else {
            uint64_t count = std::chrono::duration_cast<std::chrono::nanoseconds> (end - start).count();
            message = _Name() + ": Solved Tetravex in " + std::to_string(count) + "ns = " + std::to_string((float)(count) / 1000000000.0f) + "s";
        }
        renderer->RenderRoutine(message);
    } catch(Exception& e) {
        renderer->RenderRoutine(e.what());
    }
}

std::string SequentialSolver::_Name() {
    return "SEQUENTIAL SOLVER";
}

bool SequentialSolver::_IsValid(const unsigned& position, const Ptr_Piece& piece) {
    unsigned length = board->GetLength();
    // Check top
    if(position > length-1 && b[position - length] != nullptr && piece->GetTop() != b[position - length]->GetBottom()) {
        return false;
    }
    // Check left
    if(position % length != 0 && b.at(position-1) != nullptr && piece->GetLeft() != b[position-1]->GetRight()) {
        return false;
    }
    return true;
}

bool SequentialSolver::_Backtracking(const unsigned& position) {
    if(position == board->GetPieceCount()) {
        board->SetOnboardPieces(b);
        return true;
    }

    for(auto& piece : P) {
        if(!piece->used && _IsValid(position, piece)) {
            b[position] = piece;
            piece->used = true;
            if(_Backtracking(position+1)) {
                return true;
            }
            piece->used = false;
        }
    }
    return false;
}