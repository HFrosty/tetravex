//
// Created by AURELIEN on 16/10/2020.
//

#include "Tetravex/Piece.h"

Piece::Piece(unsigned int top, unsigned int left, unsigned int right, unsigned int bottom) {
    this->top = top;
    this->left = left;
    this->right = right;
    this->bottom = bottom;
    used = false;
}

unsigned Piece::GetTop() {
    return top;
}

unsigned Piece::GetLeft() {
    return left;
}

unsigned Piece::GetRight() {
    return right;
}

unsigned Piece::GetBottom() {
    return bottom;
}
