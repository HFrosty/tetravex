//
// Created by AURELIEN on 16/10/2020.
//

#include <Tetravex/Game.h>
#include <Tetravex/BoardFactory.h>

Game::Game(std::vector<std::string> arguments) {
    if(arguments.size() <= 1) { // Game crash if no input file has been given
        throw Exception("ERROR: Tetravex needs one argument to define Tetravex file.");
    }
    std::srand(std::time(NULL));
    auto factory = BoardFactory();
    board = new Board(factory.CreateFromSourceFile(arguments[1]));
    window = new sf::RenderWindow(sf::VideoMode(1280, 960), "Tetravex");
    renderer = new Renderer(window, board);
    controller = new Controller(board, renderer);
}

void Game::Run() {
    renderer->RenderRoutine("Tetravex started");
    while(window->isOpen()) {
        sf::Event event;
        window->waitEvent(event);
        if (event.type == sf::Event::Closed) {
            window->close();
        } else {
            controller->Update(event);
        }
    }
}