#include <Tetravex/ThreadPool.h>

ThreadPool::ThreadPool(const unsigned int& threadCount) {
    threads.resize(threadCount);
    stop = false;
}

void ThreadPool::Add(Job job) {
    mutex.lock();
    jobs.push(job);
    mutex.unlock();
    cv.notify_all();
}

void ThreadPool::Start() {
    auto threadRoutine = [this]() {
        while(!stop) {
            auto lock = std::unique_lock<std::mutex>(mutex);
            cv.wait(lock, [this]() {return !jobs.empty() || stop;});
            if(stop) {
                return;
            }
            auto job = jobs.front();
            jobs.pop();
            lock.unlock();
            cv.notify_all();
            job();
        }
    };

    for(unsigned int i = 0; i < this->threads.size(); ++i) {
        this->threads[i] = std::thread(threadRoutine);
    }
}

void ThreadPool::Stop() {
    stop = true;
}

void ThreadPool::WaitEnd() {
    for(unsigned int i = 0; i < this->threads.size(); ++i) {
        if(this->threads[i].joinable()) {
            this->threads[i].join();
        }
    }
    return;
}