//
// Created by AURELIEN on 16/10/2020.
//

#include <Tetravex/Board.h>

Board::Board(const std::string& filename, const std::vector<Piece>& pieces) {
    this->filename = filename;
    size = pieces.size();
    length = std::sqrt(pieces.size());
    original.resize(pieces.size());
    for(unsigned i = 0; i < original.size(); ++i) {
        original[i] = std::make_shared<Piece>(pieces[i]);
    }
    onboard = original;
}

void Board::Reset() {
    onboard = original;
}

void Board::SetOnboardPieces(const std::vector<Ptr_Piece>& pieces) {
    onboard = pieces;
}

std::string Board::GetFilename() const {
    return filename;
}

unsigned Board::GetLength() const {
    return length;
}

unsigned Board::GetPieceCount() const {
    return size;
}

const std::vector<Ptr_Piece>& Board::GetOriginalPieces() const {
    return original;
}

const std::vector<Ptr_Piece>& Board::GetOnboardPieces() const {
    return onboard;
}