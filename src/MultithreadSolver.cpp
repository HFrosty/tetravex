//
// Created by AURELIEN on 08/11/2020.
//

#include <Tetravex/MultithreadSolver.h>

std::string MultithreadSolver::_Name() {
    return "MULTITHREAD SOLVER";
}

bool MultithreadSolver::_Backtracking(const unsigned& position) {

    auto work = [this](unsigned start, unsigned stop) {
        auto pCopy = _Copy(this->P);
        auto bCopy = _Copy(this->b);
        for(unsigned position = start; position < stop; ++position) {
            auto piece = pCopy[position];
            bCopy[0] = piece;
            piece->used = true;
            if(_SubBacktracking(std::ref(pCopy), std::ref(bCopy), 1)) {
                return true;
            }
            piece->used = false;
        }
        return false;
    };

    std::vector<std::thread> threads;
    unsigned maxThread = std::thread::hardware_concurrency();
    unsigned leftover = board->GetPieceCount() % maxThread;
    unsigned offset = 0;
    for(unsigned i = 0; i < maxThread; ++i) {
        unsigned start = board->GetPieceCount() / maxThread * i + offset;
        unsigned end = board->GetPieceCount() / maxThread * (i + 1) + offset;
        if(leftover != 0) {
            offset++;
            ++end;
            --leftover;
        }
        threads.push_back(std::thread(work, start, end));
    }

    for(auto& thread : threads) {
        if(thread.joinable()) {
            thread.join();
        }
    }

    return solved;
}

bool MultithreadSolver::_SubBacktracking(std::vector<Ptr_Piece>& pCopy, std::vector<Ptr_Piece>& bCopy, unsigned position) {
    if(solved) {
        return true;
    }

    if(position == board->GetPieceCount()) {
        board->SetOnboardPieces(bCopy);
        solved = true;
        return true;
    }

    for(auto& piece : pCopy) {
        if(!piece->used && _IsValid(position, piece, bCopy, board->GetLength())) {
            bCopy[position] = piece;
            piece->used = true;
            if(_SubBacktracking(std::ref(pCopy), std::ref(bCopy), position+1)) {
                return true;
            }
            piece->used = false;
        }
    }
    return false;
}

std::vector<Ptr_Piece> MultithreadSolver::_Copy(const std::vector<Ptr_Piece>& original) {
    std::vector<Ptr_Piece> copy;
    copy.resize(original.size(), nullptr);
    for(unsigned i = 0; i < original.size(); ++i) {
        if(original[i] == nullptr) {
            break;
        }
        copy[i] = std::make_shared<Piece>(Piece(*original[i]));
    }
    return copy;
}

bool MultithreadSolver::_IsValid(const unsigned& position, const Ptr_Piece& piece, const std::vector<Ptr_Piece>& b, const unsigned& length) {
    // Check top
    if(position > length-1 && b[position - length] != nullptr && piece->GetTop() != b[position - length]->GetBottom()) {
        return false;
    }
    // Check left
    if(position % length != 0 && b.at(position-1) != nullptr && piece->GetLeft() != b[position-1]->GetRight()) {
        return false;
    }
    return true;
}