//
// Created by AURELIEN on 09/11/2020.
//

#include <Tetravex/PoolSolver.h>

PoolSolver::PoolSolver(Board* board, Renderer* renderer) : SequentialSolver(board, renderer), pool(std::thread::hardware_concurrency()) {
}

std::string PoolSolver::_Name() {
    return "POOL SOLVER";
}

bool PoolSolver::_Backtracking(const unsigned& position) {
    auto job = [this](unsigned position) {
        auto pCopy = MultithreadSolver::_Copy(this->P);
        auto bCopy = MultithreadSolver::_Copy(this->b);
        auto piece = pCopy[position];
        bCopy[0] = piece;
        piece->used = true;
        if(_SubBacktracking(std::ref(pCopy), std::ref(bCopy), 1)) {
            return true;
        }
        piece->used = false;
        return false;
    };

    pool.Start();
    for(unsigned i = 0; i < board->GetPieceCount(); ++i) {
        pool.Add(std::bind(job, i));
    }
    pool.WaitEnd();
    return solved;
}

bool PoolSolver::_SubBacktracking(std::vector<Ptr_Piece>& pCopy, std::vector<Ptr_Piece>& bCopy, unsigned position) {
    if(solved) {
        return true;
    }

    if(position == board->GetPieceCount()) {
        pool.Stop();
        board->SetOnboardPieces(bCopy);
        solved = true;
        return true;
    }

    for(auto& piece : pCopy) {
        if(!piece->used && MultithreadSolver::_IsValid(position, piece, bCopy, board->GetLength())) {
            bCopy[position] = piece;
            piece->used = true;
            if(_SubBacktracking(std::ref(pCopy), std::ref(bCopy), position+1)) {
                return true;
            }
            piece->used = false;
        }
    }
    return false;
}