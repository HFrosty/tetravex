//
// Created by AURELIEN on 16/10/2020.
//

#include "Tetravex/Renderer.h"

const std::map<unsigned, sf::Color> Renderer::colorMap = {
        {0, sf::Color(255, 255, 255)},
        {1, sf::Color(255, 0, 0)},
        {2, sf::Color(0, 255, 0)},
        {3, sf::Color(0, 0, 255)},
        {4, sf::Color(255, 255, 0)},
        {5, sf::Color(255, 0, 255)},
        {6, sf::Color(0, 255, 255)},
        {7, sf::Color(255, 128, 64)},
        {8, sf::Color(64, 255, 128)},
        {9, sf::Color(128, 64, 255)}
};

Renderer::Renderer(sf::RenderWindow *render, Board* board) {
    this->render = render;
    this->board = board;
    if(!font.loadFromFile("../gamedata/arial.ttf")) {
        throw Exception(std::string("Unable to load arial.ttf font file in gamedata folder!"));
    }
}

void Renderer::Clear() {
    render->clear(sf::Color(0, 0, 0));
}

void Renderer::RenderRoutine(const std::string &headline) {
    Clear();
    _RenderBoard();
    _RenderInstructions();
    _RenderHead(headline);
    render->display();
}

void Renderer::_RenderBoard() {
    unsigned size = 960 / board->GetLength() / 2;
    unsigned x = size;
    unsigned y = (960 / 2) - (size * board->GetLength() / 2);
    unsigned i = 0;
    for(auto& piece : board->GetOnboardPieces()) {
        if(i != 0 && i % board->GetLength() == 0) {
            x = size;
            y += size - 1;
        }
        if(piece != nullptr) {
            _DrawPiece(*piece, x, y, size);
        }
        x += size - 1;
        ++i;
    }
}

void Renderer::_RenderInstructions() {
    sf::Text text;
    text.setFont(font);
    text.setCharacterSize(24);
    text.setFillColor(sf::Color::White);
    text.setStyle(sf::Text::Bold);

    std::string lines[] = {
        "Filename: " + board->GetFilename(),
        "A Key: Sequential Solver",
        "Z Key: Multithread Solver",
        "E Key: Pool Solver",
        "R Key: Reset",
        "Q Key: Quit"
    };

    unsigned int i = 0;
    for(auto line : lines) {
        text.setString(line);
        text.setPosition(1280 / 2 - 60, 260 + 36 * i);
        render->draw(text);
        ++i;
    }
}

void Renderer::_RenderHead(const std::string& value) {
    sf::Text text;
    text.setFont(font);
    text.setCharacterSize(24);
    text.setFillColor(sf::Color::White);
    text.setStyle(sf::Text::Bold);
    text.setString(value);
    text.setPosition(64, 64);
    render->draw(text);
}

void Renderer::_DrawPiece(Piece piece, unsigned x, unsigned y, unsigned size) {
    auto half = size / 2;
    unsigned values[] = {piece.GetTop(), piece.GetLeft(), piece.GetRight(), piece.GetBottom()};

    sf::ConvexShape shape;
    shape.setFillColor(colorMap.at(values[0]));
    shape.setPointCount(3);
    shape.setPoint(0, sf::Vector2f(x - half, y - half));
    shape.setPoint(1, sf::Vector2f(x + half, y - half));
    shape.setPoint(2, sf::Vector2f(x, y));
    render->draw(shape);

    shape.setFillColor(colorMap.at(values[1]));
    shape.setPointCount(3);
    shape.setPoint(0, sf::Vector2f(x - half, y - half));
    shape.setPoint(1, sf::Vector2f(x - half, y + half));
    shape.setPoint(2, sf::Vector2f(x, y));
    render->draw(shape);

    shape.setFillColor(colorMap.at(values[2]));
    shape.setPointCount(3);
    shape.setPoint(0, sf::Vector2f(x + half, y - half));
    shape.setPoint(1, sf::Vector2f(x + half, y + half));
    shape.setPoint(2, sf::Vector2f(x, y));
    render->draw(shape);

    shape.setFillColor(colorMap.at(values[3]));
    shape.setPointCount(3);
    shape.setPoint(0, sf::Vector2f(x - half, y + half));
    shape.setPoint(1, sf::Vector2f(x + half, y + half));
    shape.setPoint(2, sf::Vector2f(x, y));
    render->draw(shape);
}