//
// Created by AURELIEN on 16/10/2020.
//

#include <Tetravex/BoardFactory.h>

Board BoardFactory::CreateFromSourceFile(std::string source) {
    std::ifstream file;
    file.open(source);
    if(!file.is_open()) {
        throw Exception("Unable to open file " + source + "\n");
    }
    // Fetch tetravex size
    unsigned size;
    file >> size; file >> size;
    // Prepare vector
    std::vector<Piece> pieces;
    pieces.resize(size * size, Piece(0, 0, 0, 0));
    // Fetch each square data
    for(unsigned i = 0; i < size*size; ++i) {
        unsigned top, left, right, bottom;
        file >> left;
        file >> top;
        file >> right;
        file >> bottom;
        pieces[i] = Piece(top, left, right, bottom);
    }
    return Board(source, pieces);
}

Board BoardFactory::CreateDummy(unsigned size) {
    std::vector<Piece> pieces;
    for(unsigned i = 0; i < size*size; ++i) {
        pieces.push_back(Piece(std::rand() % 10, std::rand() % 10, std::rand() % 10, std::rand() % 10));
    }
    return Board("DUMMYFILE", pieces);
}
