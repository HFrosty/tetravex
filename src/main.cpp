#include <Tetravex/Game.h>
#include <unistd.h>

int main(int argc, char* argv[]) {
    try {
        std::vector<std::string> arguments;
        arguments.resize(argc);
        for(int i = 0; i < argc; ++i) {
            arguments[i] = std::string(argv[i]);
        }
        if(argc == 1) {
            arguments.push_back("../gamedata/5x5.vex");
        }
        Game game = Game(arguments);
        game.Run();
    } catch(Exception& e) {
        std::cout << "UNCAUGHT EXCEPTION:\n" << e.what() << std::endl;
    }
    return 0;
}
