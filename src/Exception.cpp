#include <Tetravex/Exception.h>

Exception::Exception(const std::string& message) {
    this->message = message;
}

const char* Exception::what() const throw() {
    return message.c_str();
}