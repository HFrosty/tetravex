//
// Created by AURELIEN on 16/10/2020.
//

#include "Tetravex/Controller.h"

Controller::Controller(Board *board, Renderer *renderer) {
    this->board = board;
    this->renderer = renderer;
}

void Controller::Update(const sf::Event& event) {
    if(event.type != sf::Event::TextEntered) {
        return;
    }
    if(sf::Keyboard::isKeyPressed(sf::Keyboard::A)) {
        SequentialSolver solver(board, renderer);
        solver.Solve();
    } else if(sf::Keyboard::isKeyPressed(sf::Keyboard::Z)) {
        MultithreadSolver solver(board, renderer);
        solver.Solve();
    } else if(sf::Keyboard::isKeyPressed(sf::Keyboard::E)) {
        PoolSolver solver(board, renderer);
        solver.Solve();
    } else if(sf::Keyboard::isKeyPressed(sf::Keyboard::R)) {
        board->Reset();
        renderer->RenderRoutine("Reset");
    } else if(sf::Keyboard::isKeyPressed(sf::Keyboard::Q)) {
        exit(0);
    }

}
