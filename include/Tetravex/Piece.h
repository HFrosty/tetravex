//
// Created by AURELIEN on 16/10/2020.
//

#ifndef TETRAVEX_PIECE_H
#define TETRAVEX_PIECE_H

class Piece {
public:
    Piece(unsigned top, unsigned left, unsigned right, unsigned bottom);

    unsigned GetTop();
    unsigned GetLeft();
    unsigned GetRight();
    unsigned GetBottom();
    bool used;

private:
    unsigned top;
    unsigned left;
    unsigned right;
    unsigned bottom;

};


#endif //TETRAVEX_PIECE_H
