//
// Created by AURELIEN on 09/11/2020.
//

#ifndef TETRAVEX_POOLSOLVER_H
#define TETRAVEX_POOLSOLVER_H

#include <Tetravex/MultithreadSolver.h>
#include <Tetravex/ThreadPool.h>

class PoolSolver : public SequentialSolver {
public:
    PoolSolver(Board* board, Renderer* renderer);
    virtual std::string _Name();
    virtual bool _Backtracking(const unsigned& position);
    virtual bool _SubBacktracking(std::vector<Ptr_Piece>& P, std::vector<Ptr_Piece>& b, unsigned position);

private:
    ThreadPool pool;
};
#endif //TETRAVEX_POOLSOLVER_H