//
// Created by AURELIEN on 16/10/2020.
//

#ifndef TETRAVEX_BOARD_H
#define TETRAVEX_BOARD_H

#include <Tetravex/Piece.h>

using Ptr_Piece = std::shared_ptr<Piece>;

class Board {

public:
    explicit Board(const std::string& filename, const std::vector<Piece>& pieces);

    void Reset();
    void SetOnboardPieces(const std::vector<Ptr_Piece>& pieces);

    std::string GetFilename() const;
    unsigned GetLength() const;
    unsigned GetPieceCount() const;
    const std::vector<Ptr_Piece>& GetOriginalPieces() const;
    const std::vector<Ptr_Piece>& GetOnboardPieces() const;

private:
    unsigned size;
    unsigned length;
    std::string filename;
    std::vector<Ptr_Piece> original;
    std::vector<Ptr_Piece> onboard;
};


#endif //TETRAVEX_BOARD_H
