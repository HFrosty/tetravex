//
// Created by AURELIEN on 08/11/2020.
//

#ifndef TETRAVEX_MULTITHREADSOLVER_H
#define TETRAVEX_MULTITHREADSOLVER_H

#include <Tetravex/SequentialSolver.h>

class MultithreadSolver : public SequentialSolver {
using SequentialSolver::SequentialSolver;

public:
    virtual std::string _Name();
    virtual bool _Backtracking(const unsigned& position);
    bool _SubBacktracking(std::vector<Ptr_Piece>& P, std::vector<Ptr_Piece>& b, unsigned position);
    static std::vector<Ptr_Piece> _Copy(const std::vector<Ptr_Piece>& original);
    static bool _IsValid(const unsigned& position, const Ptr_Piece& piece, const std::vector<Ptr_Piece>& b, const unsigned& length);
};
#endif //TETRAVEX_MULTITHREADSOLVER_H