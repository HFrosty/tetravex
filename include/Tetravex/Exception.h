//
// Created by AURELIEN on 16/10/2020.
//

#ifndef TETRAVEX_EXCEPTION_H
#define TETRAVEX_EXCEPTION_H

class Exception : std::exception {
public:
    Exception(const std::string& message);

    virtual const char* what() const throw();
private:
    std::string message;
};


#endif //TETRAVEX_EXCEPTION_H
