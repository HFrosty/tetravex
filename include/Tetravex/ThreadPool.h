
#ifndef TETRAVEX_THREADPOOL_H
#define TETRAVEX_THREADPOOL_H

class ThreadPool {
    using Job = std::function<void()>;

public:
    ThreadPool(const unsigned int& threadCount);
    void Add(Job job);
    void Start();
    void Stop();
    void WaitEnd();
private:
    bool stop;
    std::mutex mutex;
    std::condition_variable cv;
    std::vector<std::thread> threads;
    std::queue<Job> jobs; // List of jobs
};
#endif //TETRAVEX_THREADPOOL_H