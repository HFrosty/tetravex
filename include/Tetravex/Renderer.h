//
// Created by AURELIEN on 16/10/2020.
//

#ifndef TETRAVEX_RENDERER_H
#define TETRAVEX_RENDERER_H

#include <Tetravex/Board.h>
#include <Tetravex/Exception.h>

class Renderer {
public:
    Renderer(sf::RenderWindow* render, Board* board);

    void Clear();
    void RenderRoutine(const std::string &headline);

private:
    void _RenderBoard();
    void _RenderInstructions();
    void _RenderHead(const std::string& value);
    void _DrawPiece(Piece piece, unsigned x, unsigned y, unsigned size);

    static const std::map<unsigned, sf::Color> colorMap;

    sf::RenderWindow* render;
    sf::Font font;
    Board* board;
};


#endif //TETRAVEX_RENDERER_H
