//
// Created by AURELIEN on 16/10/2020.
//

#ifndef TETRAVEX_BOARDFACTORY_H
#define TETRAVEX_BOARDFACTORY_H

#include <Tetravex/Board.h>
#include <Tetravex/Exception.h> 

class BoardFactory {
public:
    Board CreateFromSourceFile(std::string source);
    Board CreateDummy(unsigned size);
};


#endif //TETRAVEX_BOARDFACTORY_H
