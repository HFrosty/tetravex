//
// Created by AURELIEN on 16/10/2020.
//

#ifndef TETRAVEX_GAME_H
#define TETRAVEX_GAME_H

#include <Tetravex/Controller.h>
#include "Renderer.h"
#include <Tetravex/Exception.h>

class Game {
public:
    Game(std::vector<std::string> arguments);
    void Run();

private:
    sf::RenderWindow* window;
    Board* board;
    Controller* controller;
    Renderer* renderer;
};


#endif //TETRAVEX_GAME_H
