//
// Created by AURELIEN on 16/10/2020.
//

#ifndef TETRAVEX_SEQUENTIALSOLVER_H
#define TETRAVEX_SEQUENTIALSOLVER_H

#include <Tetravex/Exception.h>
#include <Tetravex/Board.h>
#include <Tetravex/Renderer.h>

class SequentialSolver {

public:
    explicit SequentialSolver(Board *board, Renderer *renderer);
    virtual void Solve();

protected:
    Board* board;
    Renderer* renderer;
    std::vector<Ptr_Piece> P;
    std::vector<Ptr_Piece> b;
    bool solved;
    unsigned test = 0;

    virtual std::string _Name();
    virtual bool _IsValid(const unsigned& position, const Ptr_Piece& piece);
    virtual bool _Backtracking(const unsigned& position);
};


#endif //TETRAVEX_SEQUENTIALSOLVER_H
