//
// Created by AURELIEN on 16/10/2020.
//

#ifndef TETRAVEX_CONTROLLER_H
#define TETRAVEX_CONTROLLER_H

#include <Tetravex/Renderer.h>
#include <Tetravex/Board.h>
#include <Tetravex/SequentialSolver.h>
#include <Tetravex/MultithreadSolver.h>
#include <Tetravex/PoolSolver.h>

class Controller {
public:
    explicit Controller(Board *board, Renderer *renderer);

    void Update(const sf::Event& event);
private:
    Board* board;
    Renderer* renderer;
};


#endif //TETRAVEX_CONTROLLER_H
