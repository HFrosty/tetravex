cmake_minimum_required(VERSION 3.17)
project(Tetravex)

include_directories(include)
link_directories(lib)

set(CMAKE_CXX_STANDARD 20)

file(GLOB SOURCE_FILES "src/*.cpp")
add_executable(Tetravex ${SOURCE_FILES})
if( WIN32 )
    target_link_libraries(Tetravex sfml-graphics-d sfml-window-d sfml-system-d)
else()
    target_link_libraries(Tetravex sfml-graphics sfml-window sfml-system pthread)
    set(CMAKE_CXX_FLAGS "-Wall -Wextra -lpthread")
    set(CMAKE_CXX_FLAGS_DEBUG "-g")
    set(CMAKE_CXX_FLAGS_RELEASE "-O3")
endif()

# Precompiled Headers
set(STL_HEADERS "<string>" "<iostream>" "<vector>" "<map>" "<chrono>" "<cstdint>" "<exception>" "<set>" "<sstream>" "<unordered_set>" "<stack>" "<fstream>" "<cmath>" "<memory>" "<thread>" "<functional>" "<mutex>" "<queue>" "<condition_variable>" "<unistd.h>" "<mutex>")
set(SFML_HEADERS "<SFML/Graphics.hpp>" "<SFML/Window.hpp>" "<SFML/System.hpp>")
target_precompile_headers(Tetravex PRIVATE ${STL_HEADERS} ${SFML_HEADERS})